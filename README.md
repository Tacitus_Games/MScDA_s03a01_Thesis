# Thesis for MSc in Data Analytics.  
**Domain:** Impact of social housing on Ireland's housing crisis.

## Description

This report examines the causal factors of the housing crisis in Ireland, 
investigating the claims made through a wide body of literature and ultimately 
analysing the impact and importance of social housing on general rental 
affordability specifically. While there is much academic literature on the 
subject there is little in the way of quantitative data analysis detailing the 
links between affordability and social housing, or any causal factor for that 
matter. This report finds a very strong correlation between the amount of new 
social housing built and the affordability of broader private rental costs in 
the following years, backed up by research by several studies such as Byrne & 
Norris (2017). The findings of the report also show that the Irish governments 
Housing For All plan is too little, too late, providing only a mitigating effect 
against the steep decline in affordability observed since the recession. This 
indicates that a much more rigorous and broad plan of action will be needed to 
increase housing supply, decrease rental costs, and increase incomes in order to 
dramatically improve the affordability of private rental accommodation within 
the Irish housing market so that it can, at least, return to the levels that it 
was at a decade ago.

## Authors and acknowledgment
Tacitus

## License
Copyright © 2023 Tacitus, all rights reserved
